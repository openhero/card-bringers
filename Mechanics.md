# Mechanics

Mechanics are the moving parts of a Card Bringers Set - these are the Creature Types, Abilities, Actions, and Supports.

The below definitions form part of the "Base Working Title". See [Playbooks and Titles](./Playbooks_and_Titles.md)

## Creature Types

Creature Types allow Abilities, Actions and Supports to be selectively targeting.

Types are divided into two Archetype categories: Kin and Classes. Creatures have one of each. Cards may target either Kin or Class.

Each Archetype Kin or Class may have Subtypes. Cards targeting Archetypes affect any Subtypes. Cards Targeting Subtypes only affect the Subtype in question.

Conjuries and Abilities targeting a Subtype incur -1 AC.

### Races

* Folk - optional subtypes of Human, Dwarf, Elf
* Beastkin - optional subtypes of Aquatic or Aerial
* Treekin - optional subtypes of Rooted, Wandering
* Eldritch - optional subtypes of Undead, Horror
* Spiritual - optional subtypes of Holy, Mythical
* Elemental - required subtypes of Fire, Water, Earth, Air

### Classes

Classes may specify an Ability for a modified AC (rounded up) if the conditions match. Using this Ability on a Creature specified with the given Class is optional. Using such an Ability occupies the Creature's Ability slot.

* Fighter : 1/2 AC "On Enter (Damage)"
* Ranger : -1 AC "Accompanied 2 (Beastkin)"
* Rogue : -1 AC "On Death (Predate)" (single Creature)
* Druid: -1 AC "Flanked 2 (Treekin)"
* Sorcerer: -1 AC "Enter (Damage) (any single non-Elemental target)"
* Healer: "Cheer (Support) (two Types)"

## Supports

A Support modifies the ATK/DEF stats of a Creature.

This can be +N or -N for any stat.

Supports are set in Conjuries or Omens, or used in Abilities. When setting a Support, the Card must specify types to apply to, and incurs +1 Additional Cost for every three Types, from the third Type onwards.

## Abilities

Each Ability has a name, and can only exist on a Creature.

Some Abilities may incur an Additional Cost (AC) to be added to a Creature's Base Cost (BC).

### Fixed Cost Abilities

#### Primary abilities

The following Abilities all incur a plain +1 AC

* Deadly - kills anything it deals Combat Damage to
* Threatening - requires two or more blockers
* Healing - gives 1 HP to controlling player per ATK point when dealing Damage to another Creature
* Raring - allows the Creature to attack in the same Turn it was summoned
* Dilligent - Creature returns to Resting Spot at end of Turn
* Outstretched - can block Flying
* Flying - can only be blocked by Flying or Outretched

#### Secondary Abilities (imply the invocation of another Ability or Action)

* Mentor (Ability) - This Creature, and and all Creatures sharing any of this Creature's Types, benefit from the stated (Ability)
    * +2 AC
* Talented (Ability 1), (Ability 2) - a Creature as the two stated Abilities.
    * +2 AC
* On Attacking (Action) - when the Creature enters the Field, Action is carried out
    * +1 AC for "On Attacking", + AC cost of the Action
* On Blocking (Action) - when the Creature enters the Field, Action is carried out
    * +1 AC for "On Blocking", + AC cost of the Action
* On Entering (Action) - when the Creature enters the Field, Action is carried out
    * -0 AC for "On Entering", + AC cost of the Action
* On Death (Action) - when the Creature dies, the Action is carried out
    * +0 AC for "On Death", + AC cost of the Action
* Trap (Action) - sacrifice the Creature, perform the Action
    * +0 AC for "Trap", + AC cost of the Action
* Cheer (Type) (Support) - applies the Support to Creatures with specified (Type)

### Variable Cost Abilities

Some Abilities may have an effect for N - this is used to calculate the cost

* Accompanied (N), (Types) - When the Creature enters the Field, create N 1/1 Creature Tokens with Types
    * +1 AC for every token pair

* Flanked (N), (Types), (Ability) - When the Creature enters the Field, create N 1/1 Creature Tokens, each with Types and Ability
    * +1 AC for every token

## Actions

Actions have an effect on Cards on the Field. Each Action has an N value, which corresponds to its Cost.

Actions may target any of the following amount of cards, and are marked in the below costings with a cost descriptor:

* a single card, with restrictor ("limit")
* a single card, with no restrictor ("norm")
* multiple cards, with restrictor ("multi")

A "Restrictor" limits the scope of the Action to cards meeting certain conditions:

* A stat above/below a certain value
    * "lower than" must be at most 4
    * "higher than" must be at least 4
* Any of a selection of Types

Cards that target multiple cards may target Field Cards by a single player, or all players, with the following costing adjustment:

* "Cards the Player controls" : +0 AC
* "Cards an Opponent controls" : +0 AC
* "All Cards on the Field" : -1 AC

Items marked "N/A" are Not Allowed for that Action type.


```
                         TARGETING
                    limit   norm   multi

Destroy              1        2      3
    Kills Creatures (mechanics on death apply)

Predate              0        1      N/A
    Kills Creatures of DEF <= to this Creature's ATK (mechanics on death apply)

Deny                 2        3      4
    Moves Field Creatures to the Discard Pile, without triggering death mechanics

Call In (N, Types)   N+3      N/A    N/A
    Bring in N 1/1 tokens with Types

Damage N             N-1      N      N+1
    Deals non-Combat damage to Creatures

Return N to hand     N+2      N+3    N/A
    Returns Creatures to hand from Field

Retrieve N           N*4      N/A    N/A
    Pick N cards from Discard Pile, must carry a condition

Resurrect            N/A      3      N/A
    Bring back a Creature that was placed in the Discard pile this Turn

Draw N               N+2      N/A    N/A
    Draw N Cards from the top of the Main Draw Deck

Absolve              0        N/A    N/A
    Gain the Worth of one Omen, without dispelling it

Launch               1        2      N/A
    Sacrifice a Creature. It deals that Creature's Damage to any target as non-Combat damage

Hobble (Ability)     2        3      N/A
    Removes the stated Ability
```
