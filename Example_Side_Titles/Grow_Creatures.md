# Grow Creatures

I was thinking, maybe something that can cause a progression on a Creature would make for interestingly big and scary things to appear?

Loosely inspired by _Magic the Gathering_'s _Ikoria: Lair of Behemoths_ and _Pokémon_'s _Evolution_.


## Card Types

### Omens

**Prohpecy Omens**

A Prophecy Omen is a Creature Omen that can be played on the Field without a attaching to Creature. It targets Types, and confers Support or Ability. It is not a Field Omen, and does not confer any properties unless attached to a Creature.

A Prohpecy Omen may become a normal Creature Omen when a Player attaches the Omen to a Creature. This is done at any point the Player could cast a Conjury. Attaching a Prophecy Omen to a Creature is done at no cost.

A Prohpecy Omen's Worth is base 2 AC, plus the AC of the Support or Ability being conferred.


## Mechanics

## Special Effect: Fusion

Two Creatures on the Field may be Fused as a single Creature via the `Fusion` Action.

The resulting Creature has ATK equal to the sum of the two fused Creatures, and likewise for DEF. The resulting Creature has all the Types and Abilities of the combined Creatures.

When applying Fusion to Creatures, stack the Creature Cards together. If the Action that invoked Fusion was a Conjury or Omen, stack this with the new Fusion Creature.

A Creature that is created from Fusion does not newly Enter the Field, therefore it does not trigger any "On Entering" Mechanics. A newly Fused Creature may attack in the same turn's Combat Step, if both component Creatures have been on the Field since the previous Turn.

Some Actions move single creatures to different areas, like to Hand or Discard Pile. In these cases, the following rules apply:

* Fused Creatures are un-Fusioned and moved to the new Area as separate Creatures
* The Action Card that applied Fusion may moved separately
    * If it is a non-Creature, it is always moved to the Discard Pile
    * If it is a Creature, and that Creature was in the Fusion, it is moved to the new Area
    * If it is a Creature and that Creature remains independent, it is unaffected

### Abilities

### Primary Abilities

* Survivor - `Destroy` Action can only reduce the Creature's DEF to 1

### Actions

```
                           TARGETS
                    limit   norm   multi
Fusion (A, B)         2      3     N/A
    Combine creature A with Creature B as a single Creature
```
