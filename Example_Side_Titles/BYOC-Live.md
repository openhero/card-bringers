# Bring Your Own Content : Live

A Side Title aimed at allowing Player content to be added in the middle of play.

This intends to bridge the gap between players who desire highly customised or creative modes of playing, with those who wish to play by defined rules, and allows players to enter the world-building process of a game. For example, an extended game session might consist of alternating periods of collaborative creation, and periods of card play.

## Areas

* Decks
    * Personal Draw Deck - draw from this Deck when a particular Mechanic indicates to do so

Once exhausted, a Personal Draw Deck may not be replenished during the Game Round.


## Actions

For these actions, "Player's Personal Draw Deck" is the Personal Draw Deck of the Player who played a Card with the stated action.


```
                             TARGETING
                    limit   norm   multi   all
Draw Yours (N)      N*5      N/A    N/A    N/A
    Draw N Cards from the top of Player's Personal Draw Deck

Surprises (N)       N*3      N/A    N/A    N/A
    Draw N Cards from the top of Player's Personal Draw Deck, place the cards on top of the Discard Pile
```
