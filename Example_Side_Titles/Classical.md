# Side Title : Classical

Some light (and incomplete) theming for Classical Mythology and concepts.

A demonstration of how a Side Title may be written as a flavour extension of a game world.

## Types

### Kin

* Spiritual - subtype: Deity

### Classes

* Hero
    * -2 AC "Cheer (Folk if BC < 5)"

## Actions

```
                             TARGETING
                    limit   norm   multi
Zeus Smite (N)      N       N+1    N+3
    Strike lightning on target for N damage
```
