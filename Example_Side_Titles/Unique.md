# Side Title: Unique

This Side Title provides a Mechanism to create Creatures with an entirely custom Ability, outwith the Abilities listed in the Playbook.

This Mechanic allows for creating Cards that are truly Unique to themselves. This is intended for Set creators to tailor specific Cards to express the uniqueness of certain inhabitants of a game world.

## Abilities

The following Abilities are defined in this Side Title:

* Unique - the Card may introduce its own effects, instead of using a Playbook Mechanic, valid for that Card.
    * The Card's effect may not be confered by the "Mentor" Ability in any way.