# Card Bringers

* Ever wanted to skin a fantasy card-collector to use your favorite franchise - but the game design was too complex to get the balance right?
* Or perhaps you want some creative freedom, but fear the vast openness of table-top RPGs - along with acting in-character?
* Perhaps you want to create your own world and lore, but nobody wants to commit to fully DM-ing your game sessions ?
* Or you just want to make a game with your friends without the complex faff?

Then _Card Bringers_ was designed for you!

_Card Bringers_ aims to combine PvP card-collector battling with player creativity and collaborative world-building by providing a simple game system, with extending and sharing at the core of its design. Extend the rules by writing _Title_ handbooks, create Card Sets that use the mechanics of those _Titles_, and bring your _Titles_ and Card Sets along to play your creations with friends - novice and veteran alike!

## What is this ?

**Card Bringers** is the working name for a card-based PvP game. The inspration for it comes from general card-collector games, including _Magic the Gathering_ and _Hearthstone_, as well as from table-top RPG games like _Dungeons and Dragons_ : as such, its focus is on encouraging players to create and play their own cards - even in the middle of gameplay itself !

_Card Bringers_ aims to ensure by design that new players can be up and running fast, and understand both the mechanics of gameplay, as well as of card creation.

The rules are deliberately kept simple - the step turn rules are straightforward enough to be explainable to new players in a couple of play turns. The Mechanics, the main moving parts of the game, intend to be self-explanatory, and their construction style is aimed at setting a model for defining further Mechanics intuitively.

## Game

These are the base rules for _Card Bringers_, and its default ["Working Title."](./Mechanics.md)

The reading order below is recommended for players:

* [Playing a round](./Rules_of_turns.md) - the base rules. Start playing !
* [Game mechanics](./Mechanics.md) - understand what various cards can do, provide costing calculation formulas, and other moving-parts for a playable game

The further notes are of relevant reading for Set and Title creators:

* [Cards](./Cards.md) - Fundamental rules for Creatures, Conjuries, and Omens
* [Playbooks and Titles](./Playbooks_and_Titles.md) - about combining Working Titles and Side Titles into game session Playbooks
* [Example Side Titles](./Example_Side_Titles) - customisation documents system

## Motivation

Simply put, I wanted a fantasy game that had clear rules, but that could be easily expanded by any player, in advance or on the spot, and allowed for a creative element to be introduced.

Classic table-top role-playing games highlight the creative and collaborative aspects of playing games together, but can feel much too open-ended, or require too much spur-of-the-moment creativity and acting. This tends to exclude players who prefer either taking their time in creating their experience, or who prefer playing within the restrictions of detrerministic rules. Some people just don't like the thespian limelight.

TTRPGs also tend to be reliant on math during play (both at setup and throughout campaigns) (_Dungeons and Dragons_) or far too open to interpretation (_Fate_ system). _Card Bringers_ moves the (modest) calculation task to the creation step, outside of play, to maintain an easily-accessible play session.

On the other end, strictly Card-Collector games have a tendency to have complex rules for advanced cards, many of which are left to discovery as card packs are purchased, and driven by a central official publisher of the game. There is little scope for personal creativity, and competitive rules and revocation pronouncements are designed to direct communities to accept only sanctioned cards. Aside from that, attempting to design new cards for one's own sessions is rendered difficult as the cost calculation rules are unpublished - as such, encouraging players to create and bring their own cards can entail game-breaking costing easily creeping in - nevermind on-the-spot creativity.

_Card Bringers_ aims to put the creative process in the hands of even inexperienced players, separating the card theming (descriptions and types) and the active effects of cards (the active mechanics). Completely novice designers, like casual game players and children, who just want to "skin" the game are provided with a range of readily available Mechanics to choose from, whilst retaining cohesive playability. Conversely, those with a more fine-tuning affinity can create and discuss Mechanics for an enriched play through.

The design of _Card Bringers_ wants to encourage players to bring their own Cards and Titles to their game groups, and allows a player to bring a friend's creativity to another session, and enriching gameplay to the wider community of _Card Bringers_ players.

### Computer Game

Computer-based card-collector games make personal creativity even more difficult, with the ability to play being solely defined by the publisher. Building one's own server, and implementing the game rules, as an arduous task and not for the inexperienced. At some point, I intended to turn this table-top game into a computerized game, with a game state server component, and a battle UI client component, to allow Set and Title creators to host their own games, as well as benefiting from the creativity of other players around the Internet.

This is not an inherent goal of the project, but is nevertheless kept in mind when designing the rule engine.
