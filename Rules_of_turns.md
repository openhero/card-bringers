# Playing Rules

## Card Areas

A Card can be in a given area at any one time. They are as follows:

* Draw Deck
    * Main Draw Deck - drawn from by all Players during their Turn.
* Discard Pile
    * Cards arrive on the Discard Pile by being "destroyed" from the Field, a Creature Card dies, or directed so by a Mechanic
* Hand
    * By default, Cards are Drawn from the Main Draw Deck, into a Player's Hand.
    * The Player's Hand is by default hidden from other Players
* The Field - where cards are brought into play, applicable to all Players
    * Field Omens exist in The Field in General
* The Player's Field
    * A Player's part of the Field is divided into two areas for Creatures:
    * The Resting Spot - Creatures enter The Field here, and Creatures a Player controls return here at the start of a Player's Turn
    * The Battle Front - Creatures move from the Resting Spot to the Battle Front in order to attack or defend

## Before starting

* Ensure each Player has 5 cards in their Hand
* Shuffle any Draw Decks - Main Draw Deck and any other Draw Decks
* Decide on base Player HP - recommended is 30
    * A Player whose HP is reduced to 0 is removed from the game round.
    * The last Player to remain in the game wins the game round.
* Decide on first Player, then decide on proceeding clockwise or anti-clockwise.

## Turn Steps

A Game Round happens in multiple Turns. Each Turn proceeds with the following Turn Steps

* Ready Field
* Draw 1 card
* Play Omens
* Summon I (Creatures and Conjuries)
    * Summon Steps proceed in Cycles, each Player having one Intervention per Cycle
* Attack
    * Opponents Defend
* Summon II (Creatures and Conjuries)
    * Summon Steps proceed in Cycles, each Player having one Intervention per Cycle
* Cleanup

## Step Details

### Ready Field

Any Creatures on the Field, controlled by the Player whose Turn it is, are returned to the Resting Spot, unless a Mechanic states otherwise.

If the Main Draw Deck is empty, replenish it by shuffling the Discard Pile, and placing the shuffled cards as the new Main Draw Deck.

### Draw 1 Card

The Player whose turn it is draws 1 Card from the Main Draw Deck.

### Play Omens

Play up to 5 pts worth of Omens.

If an Omen is worth more than 5 pts, and if no Omens have yet been played this turn, pay the excess worth of the Omen to play by sacrificing one or more Omens from the field that you control. Excess Worth remaining is lost.

If 5 pts worth of Omens or more have been played this Turn, no more Omens may be played, unless otherwise stated by a Mechanic.

Field Omens apply continuously.

### Summon Step: Paying Costs with Omens

Creatures and Conjuries can be summoned by paying for their Cost using the Worth of as many Omens as desired and available.

Starting with the Player whose turn it is, Cycle round each Player, givine each an Intervention. Going round the players once is a "Cycle." There may be multiple Cycles in a Summon Step.

Omens are discarded at any time during the Summon Step for their Worth. The sum of Worths accumulates for the duration of the Summon step (across Cycles), and can be spent to pay the Cost of bringing in Creatures and Conjuries.

Each Player, at their Intervention, may optionally play 1 Conjury, unless a Mechanic has stated otherwise.

For the Player whose Turn it is, any number of Creatures may be summoned before they summon a Conjury, ending their Intervention, and passing on to the next Player's Intervention.

Once all Players decline to Summon a Conjury in a Cycle, declare end of Summon Step, and proceed to Combat step.

At the end of the Summon step, any unspent Worth is lost.

#### Summon Creatures

Play Creature cards from the Player's Hand by paying the Costs. A Creature enters the Field in the Rest Spot. A Creature may not attack in the same turn it was summoned, unless a Mechanic states otherwise.

Only the Player whose Turn it is may summon Creatures from their Hand.

If a Field Omen applies that would cause the summoned Creature to enter the Field with DEF less than or equal to zero, the Creature automatically dies, otherwise, it Survives.

* If the Creature does not Survive, it does not enter the Field.
* No "On Entering" Mechanics may trigger.
* Any "On death" Mechanics trigger.

#### Summon Conjuries

Play Conjury cards from the Player's Hand by paying the Costs. A Player may play 1 Conjury on their Intervention, unless stated otherwise by a Mechanic.

### Combat

Any Creatures in the Resting Spot since at least previous Turn can be sent to the Battle Front to attack opponent Players, unless othwerwise stated by a Mechanic.

Opponent Players can move any of their Creatures from the Resting Spot to the Battle Front to defend. Any attacking Creature may be blocked by any number of Blocking Creatures. Defender decides what order damage is applied in.

Attacking and Defending Creatures deal their ATK worth of damage to eachother's DEF, as Combat Damage.

* DEF is subtracted from ATK on both sides
* ATK is subtracted from DEF on both sides

When one attacking Creature is blocked by two or more defending Creatures, the first blocker's DEF reduces the attacker's ATK by subtraction. The attacker's remaining ATK is carried over to the encounter with the subsequent blocker.

Any Creature whose points reach zero dies. Any Creature that remains on the table has that fewer ATK points and DEF points until end of Turn.

Damage received as a direct effect of one Creature blocking an attack of another Creature is considered Combat Damage. All other damage is Non-Combat Damage, unless a Mechanic states otherwise. Dying in Combat is the result of dying where the last Damage taken was Combat Damage.

Attacking Creatures that are not blocked deal damage directly to the target Player, and the value of the ATK is deducted from the Player's HP.

### Cleanup

Discard any excess cards that may have ben drawn during the Turn.

Proceed to next Player's turn. All Field Creatures' ATK and DEF are restored.
