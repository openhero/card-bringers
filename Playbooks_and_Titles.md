# Playbooks and Titles

## Working Title

A booklet called the Working Title is maintained for any created Set of Cards, alongside the standard rules.

The Working Title contains the Mechanics, the actual detailed activities that can be performed during a played game. It is used to determine what Creatures, Conjuries and Omens can be created, and how to calculate their Costs and Worths.

The [Mechanics](./Mechanics.md) document is the default Working Title. This provides enough game play mechanics that can be shared acorss _Card Bringers_ games, and Cards designed against these Mechanics can be ported from game session to game session, and game group to game group.

## Playbooks

The creation of new Mechanics usually falls to the owner of a Working Title and Card Set, but Players may also create independent Mechanics and collect them in a Side Title.

Before play session, Players must agree on whose base Working Title will be used, as well as which Side Titles to include.

A Combination of a Working Title and Side Titles is referred to as a Playbook.

## Set Creators and Play Sessions

Set Creators create and/or assemble Cards before a Session. These can be simple cards without a unifying theme, but usually gain from being part of a wider thematic - a chosen World, or Story from which the Card names are drawn.

During a play session, it may be decided that Players are allowed to bring in their own Cards, using Actions from the Playbook, to add during play or upfront.

Showing and talking through eachother's Cards and Side Title Mechanics is encouraged as a way of growing your sessions, as well as adding depth to your games over time.

## New Mechanics

Standard Types, Actions, Abilities and Supports are set out in the [Default Working Title](./Mechanics.md), and serve as a base example of how to lay these out. A Mechanic must clearly set out the scope of its applicability, and the basis for calculating its Cost.

Side Titles can introduce new Mechanics, but should not modify the Costings of Mechanics already defined in the main Working Title - this would cause the re-definition of all affected cards.

When providing defintions for Kin, a Side Title's defintions _add_ Kin and Subtypes. If an existing Kin from the Working Title features in the Side Title, the Subtypes defined in the Side Title are added to those in the Working Title.

## Creating New Cards

Cards are restricted to using Mechanics from the Play Session's Playbook.

Costing should be performed upfront, using the Playbook to calculate these. Where possible, declare Costs and Worths directly on the Card.

# Advice for First Time Creators

When you are creating for the first time, it is advised to use the Default Working Title set out in the [Mechanics document](./Mechanics.md). This ensures Cards can be used across sessions, and you can get a feel for how well they perform in general.

You may create a simple Side Title that defines some extra Kin Subtypes to add, to produce more flavour theming for your new cards. Create Cards using existing Mechanics, targeting main Creature Types for better applicability. Target your new Types sparingly, as they will likely be rarer to appear in the Main Draw Deck.

Creating new Classes, Abilities and Actions is a more involved activity, and you may want to discuss with another creator what kinds of considerations to take to ensure game balance is maintained.

To design costing, consider the general limit of 5 pts playable Worth of an Omen per Turn, what it would mean in game-play for dispelling either powerful Omens, or multiple Omens. Consider also, how easy should it be to make a powerful card? How easy should it be to play a powerful card? How to discourage proliferation of game-breaking cards? Can your Mechanic be used in conjunction with another (for good or ill to the gameplay)?
