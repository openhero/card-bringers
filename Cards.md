# Cards

Cards can be created and chosen, and collected into Sets. Cards within a Set are intended to be playable together, and may have a unifying thematic denoting their belonging together.

Aside from their purely flavour-descriptions, Cards in a Set may only use Mechanics from their associated Playbook.

In general, Cards may not introduce any new Mechanics or effects specific to that Card alone.

* Creature Cards featuring Mechanics not present in the session's Playbook can remain in play, but are played with their Base Cost alone, and their Mechanic does not apply.
* Omens and Conjuries that feature Mechanics not present in the session's Playbook should be removed during play as if they hadn't been included: when drawn, they are removed from play altogether, and the Player should draw another Card in its place.

## Creatures

Creatures have a number of properties, some flavour, some game-mechanical

* Name - a nice, thematic name
* ATK - the Creature's attack power
* DEF - the Creature's defense resilience
* Types - Kin and Class
* One optional Ability slot

The Base Cost of a Creature is the sum of ATK + DEF

The Cost of a Creature is the Base Cost plus any Additional Cost brought from additional Ablities.

If a Support reduces a Creature's DEF stat to zero, that Creature dies, and is moved to the Discard Pile.

### Creature Tokens

Creature Tokens enter the Field, brought in by a Mechanic.

Creature Tokens have ATK, DEF and Creature Types, and may have an Ability. They have no name.

Creature Tokens must be defined in the Playbook.

### Types: Kin and Class

These are descriptors that allow various Properties from Omens and Conjuries to target the Creature.

The list of Types is set in the session's [Playbook](./Playbooks_and_Titles.md): this is a [Working title](./Mechanics.md), combined with optional Side Titles.

## Conjuries

Spells that can be cast for an affect that last until end of current Turn.

These may carry one of the following effects:

* Enable an Action
* Confer a Support
* Confer an Ability

Any Creature that shares any Types listed in the Conjury's Types is affected by that Conjury.

The effects of a Conjury take effect when that Conjury is Summoned.

## Omens

Omens can be played and remain either on the Field, or on a Creature.

These may carry one of

* An Ability
* A Support

A Field Omen has a Base Worth of 2, a Creature Omen has a Base Worth of 1. Then, the Additional Cost (AC) of the Ability or Support are added, to produce the Omen's Worth.

An Omen does not need to be paid for to be played - instead, Dispelling an Omen gives the Player its Worth in points, with which to pay for other cards' Costs.

A Field Omen exists in the Field in general. A Creature Omen attaches directly to a Creature. If a Creature with a Creature Omen is moved to another Area, the Creature Omen is destroyed and moved to the Discard Pile.
